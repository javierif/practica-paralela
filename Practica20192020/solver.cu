#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>
#include "wtime.h"
#include "definitions.h"
#include "energy_struct.h"
#include "cuda_runtime.h"
#include "solver.h"

using namespace std;

/**
* Kernel del calculo de la solvation. 
*/
__global__ void escalculation (int atoms_r, int atoms_l, int nlig, float *rec_x_d, float *rec_y_d, float *rec_z_d, float *lig_x_d, float *lig_y_d, float *lig_z_d, float *ql_d, float *qr_d, float *energy_d, int nconformations){

  int index_r = blockIdx.x * blockDim.x + threadIdx.x; //indice de los atomos del receptor.
  int index_l = blockIdx.y * blockDim.y + threadIdx.y; //indice de los atomos del ligando.

  float dist, total_elec = 0, miatomo[3], elecTerm; //Declaramos las variables usadas en el codigo secuencial para realizar los calculos necesarios.
  int k = 0;

  if(index_r < atoms_r && index_l < atoms_l) //Comprobamos que no nos salimos de los indices
  {
    for (int k=0; k<nconformations; ++k){
      miatomo[0] = *(lig_x_d + (k*nlig) + index_l); //Coordenada x
      miatomo[1] = *(lig_y_d + (k*nlig) + index_l); //Coordenada y
      miatomo[2] = *(lig_z_d + (k*nlig) + index_l); //Coordenada z
      
      elecTerm = 0;
      dist = calculaDistancia(rec_x_d[index_r], rec_y_d[index_r], rec_z_d[index_r], miatomo[0], miatomo[1], miatomo[2]);
      elecTerm = (ql_d[index_l] * qr_d[index_r]) / dist;

      //Se almacena el resultado en el array total de energias.
      atomicAdd(&energy_d[k], elecTerm); //Utilizamos atomicAdd para que esta instruccon se realice en exclusion mutua, necesitamos protegerla para evitar que se corrompa el resultado.
      
    }
  }//Fin del if de comprobacion de indices.
}


/**
* Funcion para manejar el lanzamiento de CUDA 
*/
void forces_GPU_AU (int atoms_r, int atoms_l, int nlig, float *rec_x, float *rec_y, float *rec_z, float *lig_x, float *lig_y, float *lig_z, float *ql ,float *qr, float *energy, int nconformations){
	
	cudaError_t cudaStatus; //variable para recoger estados de cuda

	//seleccionamos device
	cudaSetDevice(0); //0 - Tesla K40 vs 1 - Tesla K230

	//creamos memoria para los vectores para GPU _d (device)
	float *rec_x_d, *rec_y_d, *rec_z_d, *qr_d, *lig_x_d, *lig_y_d, *lig_z_d, *ql_d, *energy_d;

	/* Reservamos memoria para GPU
   * hacemos un casting a void **
   * Manejamos los errores con cudaStatus.
   */
  
  // *** RECEPTOR ***
  cudaMalloc((void **) &rec_x_d, atoms_r * sizeof(float));  
  if (cudaStatus!=cudaSuccess) { 
    fprintf(stderr, "Error. No es posible reservar memoria en device\n");
  }

  cudaMalloc((void **) &rec_y_d, atoms_r * sizeof(float));
  if (cudaStatus!=cudaSuccess) {
    fprintf(stderr, "Error. No es posible reservar memoria en device\n");
  }

  cudaMalloc((void **) &rec_z_d, atoms_r * sizeof(float));
  if (cudaStatus!=cudaSuccess) {
    fprintf(stderr, "Error. No es posible reservar memoria en device\n");
  }

  cudaMalloc((void **) &qr_d, atoms_r * sizeof(float));
  if (cudaStatus!=cudaSuccess) {
    fprintf(stderr, "Error. No es posible reservar memoria en device\n");
  }

  
  // *** LIGANDO ***
  cudaMalloc((void **) &lig_x_d, nconformations * atoms_l * sizeof(float));
  if (cudaStatus!=cudaSuccess) {
    fprintf(stderr, "Error. No es posible reservar memoria en device\n");
  }

  cudaMalloc((void **) &lig_y_d, nconformations * atoms_l * sizeof(float));
  if (cudaStatus!=cudaSuccess) {
    fprintf(stderr, "Error. No es posible reservar memoria en device\n");
  }

  cudaMalloc((void **) &lig_z_d, nconformations * atoms_l * sizeof(float));
  if (cudaStatus!=cudaSuccess) {
    fprintf(stderr, "Error. No es posible reservar memoria en device\n");
  }

  cudaMalloc((void **) &ql_d, atoms_l * sizeof(float));
  if (cudaStatus!=cudaSuccess) {
    fprintf(stderr, "Error. No es posible reservar memoria en device\n");
  }


  // *** ARRAY DE ENERGIAS ***
  cudaMalloc((void **) &energy_d, nconformations * sizeof(float));
  if (cudaStatus!=cudaSuccess) {
    fprintf(stderr, "Error. No es posible reservar memoria en device\n");
  }
 

   /* Pasamos datos de host a device
    * Manejamos los errores con cudaStatus.
    */

   // *** RECEPTOR ***
  cudaMemcpy(rec_x_d, rec_x, atoms_r * sizeof(float), cudaMemcpyHostToDevice);
  if(cudaStatus!=cudaSuccess){
    fprintf(stderr, "Error. No es posible transferir los datos de host a device.\n");
  } 

  cudaMemcpy(rec_y_d, rec_y, atoms_r * sizeof(float), cudaMemcpyHostToDevice);
  if(cudaStatus!=cudaSuccess){
    fprintf(stderr, "Error. No es posible transferir los datos de host a device.\n");
  }

  cudaMemcpy(rec_z_d, rec_z, atoms_r * sizeof(float), cudaMemcpyHostToDevice);
  if(cudaStatus!=cudaSuccess){
    fprintf(stderr, "Error. No es posible transferir los datos de host a device.\n");
  }

  cudaMemcpy(qr_d, qr, atoms_r * sizeof(float), cudaMemcpyHostToDevice);
  if(cudaStatus!=cudaSuccess){
    fprintf(stderr, "Error. No es posible transferir los datos de host a device.\n");
  }

  
  // *** LIGANDO ***
  cudaMemcpy(lig_x_d, lig_x, nconformations * atoms_l * sizeof(float), cudaMemcpyHostToDevice);
  if(cudaStatus!=cudaSuccess){
    fprintf(stderr, "Error. No es posible transferir los datos de host a device.\n");
  }

  cudaMemcpy(lig_y_d, lig_y, nconformations * atoms_l * sizeof(float), cudaMemcpyHostToDevice);
  if(cudaStatus!=cudaSuccess){
    fprintf(stderr, "Error. No es posible transferir los datos de host a device.\n");
  }

  cudaMemcpy(lig_z_d, lig_z, nconformations * atoms_l * sizeof(float), cudaMemcpyHostToDevice);
  if(cudaStatus!=cudaSuccess){
    fprintf(stderr, "Error. No es posible transferir los datos de host a device.\n");
  }

  cudaMemcpy(ql_d, ql, atoms_l * sizeof(float), cudaMemcpyHostToDevice);
  if(cudaStatus!=cudaSuccess){
    fprintf(stderr, "Error. No es posible transferir los datos de host a device.\n");
  }


  // *** ARRAY DE ENERGIA ***  
  cudaMemcpy(energy_d, energy, nconformations * sizeof(float), cudaMemcpyHostToDevice);
  if(cudaStatus!=cudaSuccess){
    fprintf(stderr, "Error. No es posible transferir los datos de host a device.\n");
  }

	
	//Definir numero de hilos y bloques
  int threads_block = 16; //Tendremos 16 * 16, en total 256 hilos.
  int blocks_r = (int) ceil(atoms_r/threads_block); //Usamos ceil con un casting a int para redondear al entero superior. Almacenamos en blocks_r los bloques para atomos del receptor.
  int blocks_l = (int) ceil(atoms_l/threads_block); //Usamos ceil con un casting a int para redondear al entero superior. Almacenamos en blocks_l los bloques para átomos del ligando.

  printf("N. Bloques Receptor: %d\n", blocks_r);
  printf("N. Bloques Ligando:  %d\n", blocks_l);
  printf("N. Hilos por bloque: %d\n", threads_block);


  // *** GRID ***
  dim3 block (blocks_r, blocks_l);
  dim3 thread (threads_block, threads_block);


	//Llamamos a kernel
	escalculation <<<block,thread>>> (atoms_r, atoms_l, nlig, rec_x_d, rec_y_d, rec_z_d, lig_x_d, lig_y_d, lig_z_d, ql_d, qr_d, energy_d, nconformations);
	
	//control de errores kernel
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if(cudaStatus != cudaSuccess) fprintf(stderr, "Error en el kernel %d\n", cudaStatus); 

	//Traemos info al host
  cudaMemcpy(energy, energy_d, nconformations * sizeof(float), cudaMemcpyDeviceToHost);

  //Para comprobar que la ultima conformacion tiene el mismo resultado que la primera
  printf("Termino electrostatico de conformacion %d es: %f\n", nconformations-1, energy[nconformations-1]);  
  //Imprimimos por pantalla el calculo del termino electrostatico que hemos traido desde device.
	printf("Termino electrostatico %f\n", energy[0]);

	
  //Liberamos memoria reservada para GPU
  
  // *** RECEPTOR ***
  cudaFree(rec_x_d);
  cudaFree(rec_y_d);
  cudaFree(rec_z_d);
  cudaFree(qr_d);

  // *** LIGANDO ***
  cudaFree(lig_x_d);
  cudaFree(lig_y_d);
  cudaFree(lig_z_d);
  cudaFree(ql_d);

  // *** ARRAY DE ENERGIAS ***
  cudaFree(energy_d);

}

/**
* Distancia euclidea compartida por funcion CUDA y CPU secuencial
*/
__device__ __host__ extern float calculaDistancia (float rx, float ry, float rz, float lx, float ly, float lz) {

  float difx = rx - lx;
  float dify = ry - ly;
  float difz = rz - lz;
  float mod2x=difx*difx;
  float mod2y=dify*dify;
  float mod2z=difz*difz;
  difx=mod2x+mod2y+mod2z;
  return sqrtf(difx);
}



/**
 * Funcion que implementa el termino electrostático en CPU
 */
void forces_CPU_AU (int atoms_r, int atoms_l, int nlig, float *rec_x, float *rec_y, float *rec_z, float *lig_x, float *lig_y, float *lig_z, float *ql ,float *qr, float *energy, int nconformations){

	double dist, total_elec = 0, miatomo[3], elecTerm;
  int totalAtomLig = nconformations * nlig;

	for (int k=0; k < totalAtomLig; k+=nlig){
	  for(int i=0;i<atoms_l;i++){					
			miatomo[0] = *(lig_x + k + i);
			miatomo[1] = *(lig_y + k + i);
			miatomo[2] = *(lig_z + k + i);

			for(int j=0;j<atoms_r;j++){				
				elecTerm = 0;
        dist=calculaDistancia (rec_x[j], rec_y[j], rec_z[j], miatomo[0], miatomo[1], miatomo[2]);
//				printf ("La distancia es %lf\n", dist);
        elecTerm = (ql[i]* qr[j]) / dist;
				total_elec += elecTerm;
//        printf ("La carga es %lf\n", total_elec);
			}
		}
		
		energy[k/nlig] = total_elec;
		total_elec = 0;
  }
	printf("Termino electrostatico %f\n", energy[0]);
}


extern void solver_AU(int mode, int atoms_r, int atoms_l,  int nlig, float *rec_x, float *rec_y, float *rec_z, float *lig_x, float *lig_y, float *lig_z, float *ql, float *qr, float *energy_desolv, int nconformaciones) {

	double elapsed_i, elapsed_o;
	
	switch (mode) {
		case 0://Sequential execution
			printf("\* CALCULO ELECTROSTATICO EN CPU *\n");
			printf("**************************************\n");			
			printf("Conformations: %d\t Mode: %d, CPU\n",nconformaciones,mode);			
			elapsed_i = wtime();
			forces_CPU_AU (atoms_r,atoms_l,nlig,rec_x,rec_y,rec_z,lig_x,lig_y,lig_z,ql,qr,energy_desolv,nconformaciones);
			elapsed_o = wtime() - elapsed_i;
			printf ("CPU Processing time: %f (seg)\n", elapsed_o);
			break;
		case 1: //OpenMP execution
			printf("\* CALCULO ELECTROSTATICO EN OPENMP *\n");
			printf("**************************************\n");			
			printf("**************************************\n");			
			printf("Conformations: %d\t Mode: %d, CMP\n",nconformaciones,mode);			
			elapsed_i = wtime();
			forces_OMP_AU (atoms_r,atoms_l,nlig,rec_x,rec_y,rec_z,lig_x,lig_y,lig_z,ql,qr,energy_desolv,nconformaciones);
			elapsed_o = wtime() - elapsed_i;
			printf ("OpenMP Processing time: %f (seg)\n", elapsed_o);
			break;
		case 2: //CUDA exeuction
			printf("\* CALCULO ELECTROSTATICO EN CUDA *\n");
      printf("**************************************\n");
      printf("Conformaciones: %d\t Mode: %d, GPU\n",nconformaciones,mode);
			elapsed_i = wtime();
			forces_GPU_AU (atoms_r,atoms_l,nlig,rec_x,rec_y,rec_z,lig_x,lig_y,lig_z,ql,qr,energy_desolv,nconformaciones);
			elapsed_o = wtime() - elapsed_i;
			printf ("GPU Processing time: %f (seg)\n", elapsed_o);			
			break; 	
	  	default:
 	    	printf("Wrong mode type: %d.  Use -h for help.\n", mode);
			exit (-1);	
	} 		
}
